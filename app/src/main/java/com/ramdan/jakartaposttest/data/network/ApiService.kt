package com.ramdan.jakartaposttest.data.network

import com.ramdan.jakartaposttest.data.response.Data
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    companion object {
        const val BASE_URL = "https://dev-api.thejakartapost.com/v1/"
    }
    @GET("articles/seasia?")
    suspend fun getArticles(
        @Query("limit") limit: Int,
        @Query("skip") skip: Int
    ): Data
}