package com.ramdan.jakartaposttest.data.response

import com.ramdan.jakartaposttest.data.model.Articles


data class ArticlesResponse(
    val code: Int?,
    val text: Int?,
)
data class Data(
    val data: List<Articles>
)